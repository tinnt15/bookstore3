import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
user = '';
  constructor(private router: Router) { }

  ngOnInit() {
    this.checkLogin();
  }

  checkLogin(){
    this.user = JSON.parse(sessionStorage.getItem('user'));
    if(this.user == null){
      this.router.navigate(['login']);
    }
  }

  goLogout(){
    this.user = '';
    this.router.navigate(['login']);
  }

}
