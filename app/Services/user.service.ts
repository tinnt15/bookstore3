import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import{ User } from '../Models/user.model'
@Injectable()
export class UserService {
  selectedUser: User;
  constructor(private http: Http) { }

  login(InfUser : User){
    var body = JSON.stringify(InfUser);
    var headerOpetions = new Headers({ 'Content-Type' : 'application/Json'});
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers : headerOpetions });
    return this.http.post('http://localhost:64691/api/login',body, requestOptions).map(x => x.json());
  }
}
