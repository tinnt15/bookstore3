import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { ngModuleJitUrl } from '@angular/compiler';
import { FormsModule } from '@angular/forms'

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { BookcreateComponent } from './bookcreate/bookcreate.component';
import { MenuComponent } from './menu/menu.component';
import { TitleComponent } from './title/title.component';
import { MainpageComponent } from './mainpage/mainpage.component';

const routesConfig: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'bookcreate', component: BookcreateComponent},
    {path: 'login', component: LoginComponent},
    {path: '',redirectTo: '/login', pathMatch:'full'},
    {path: '**',redirectTo: '/login', pathMatch:'full'}
  ];

  @NgModule({
      imports: [RouterModule.forRoot(routesConfig),
        FormsModule
    ],
      declarations: [
          TitleComponent,
          MenuComponent,
          LoginComponent,
          BookcreateComponent,
          HomeComponent,
          MainpageComponent
      ],
      exports: [RouterModule]
  })

  export class AppRoutingModule{}