import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/user.service'
import { NgForm } from '@angular/forms'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router'
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private userservice: UserService, private toastr : ToastrService, private router: Router) { }

  ngOnInit() {
  }

  resetForm(form: NgForm) {
    form.reset();
    this.userservice.selectedUser = {
      UserName: null,
      Email: '',
      PassWord: '',
      IsActive: null
    }
  }

  onLogin(form: NgForm) {
    this.userservice.login(form.value)
      .subscribe( val => this.checkLogin(val,form.value.Email, form.value.Password)
    );
  }

  checkLogin(val: any, email, password) {
    if (val)
    {
      this.goHome(email, password);
      this.toastr.success('Login success','Login');
    }
    else
    {
      console.log('fail');
      this.toastr.error('Username or password is incorrect','Login');
    }
  }

  goHome(email, password){
    this.router.navigate(['home']);
    sessionStorage.setItem('user', JSON.stringify({Email: email, Password: password}));
  }
}
